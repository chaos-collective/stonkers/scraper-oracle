{ pkgs ? import <nixpkgs> { } }:
pkgs.mkShell {
  # Get dependencies from the main package
  inputsFrom = [ (pkgs.callPackage ./default.nix { }) ];
  # Additional tooling
  buildInputs = with pkgs; [
    go-task
    cargo
    rustc
    rust-analyzer # LSP Server
    rustfmt # Formatter
    clippy # Linter
    cargo-tarpaulin # Test coverage
    cargo-audit # Audit
  ];

  RUST_SRC_PATH = "${pkgs.rust.packages.stable.rustPlatform.rustLibSrc}";
  OPENSSL_DEV = pkgs.openssl.dev;
  PKG_CONFIG_PATH = "${pkgs.openssl.dev}/lib/pkgconfig";
  WEBDRIVER_PATH = "${pkgs.geckodriver}/bin/geckodriver";
  FIREFOX_PATH = "${pkgs.firefox}/bin/firefox";
}
