use std::error::Error;

use crate::{config::Config, scraper::extract_text};

pub struct Application;

impl Application {
    pub async fn start(&self, config: Config) -> Result<(), Box<dyn Error>> {
        let value = extract_text(&config.url, &config.selector)
            .await
            .expect("No value found");

        let value: f64 = value.trim().replace(['\u{a0}', '€'], "").trim().parse()?;

        let value_dec: i64 = (value * 100.0) as i64;

        println!("{:?}", value_dec);
        Ok(())
    }
}
