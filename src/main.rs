use application::Application;
use config::Config;
use std::error::Error;

mod application;
mod config;
mod scraper;

#[cfg(not(tarpaulin_include))]
#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let app = Application;

    let config = Config {
        url: "https://pt.wallapop.com/item/tapete-976023154".into(),
        selector: ".item-detail-price_ItemDetailPrice--standard__TxPXr".into(),
    };

    app.start(config).await?;
    Ok(())
}

#[cfg(test)]
mod tests {
    use crate::{config::Config, Application};

    #[tokio::test]
    async fn run_main() {
        let app = Application;

        let config = Config {
            url: "https://pt.wallapop.com/item/tapete-976023154".into(),
            selector: ".item-detail-price_ItemDetailPrice--standard__TxPXr".into(),
        };

        app.start(config).await.unwrap();
    }
}
