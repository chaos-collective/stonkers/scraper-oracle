use fantoccini::{error, Client, ClientBuilder, Locator};
use serde_json::{json, Map, Value};
use tokio::{
    io,
    process::{Child, Command},
};

pub async fn extract_text(url: &str, selector: &str) -> Option<String> {
    let mut webdriver_child = spawn_webdriver().ok()?;

    let client = create_client().await.ok()?;

    client.goto(url).await.ok()?;

    let text = client
        .find(Locator::Css(selector))
        .await
        .ok()?
        .text()
        .await
        .ok();

    client.close().await.ok()?;
    webdriver_child.kill().await.ok()?;
    text
}

fn spawn_webdriver() -> io::Result<Child> {
    let webdriver = env!("WEBDRIVER_PATH");
    let browser_binary = env!("FIREFOX_PATH");
    let child = Command::new(webdriver)
        .arg("--port=4444")
        .arg(format!("--binary={}", browser_binary))
        .spawn()?;
    println!("WebDriver spawned successfully");
    Ok(child)
}

async fn create_client() -> Result<Client, error::NewSessionError> {
    fn create_capabilities() -> Map<String, Value> {
        json!({
            "moz:firefoxOptions": {
                "args": ["-headless"]
            },
        })
        .as_object()
        .unwrap()
        .to_owned()
    }

    ClientBuilder::native()
        .capabilities(create_capabilities())
        .connect("http://localhost:4444")
        .await
}

#[cfg(test)]
mod tests {
    use super::extract_text;

    #[tokio::test]
    async fn it_extracts_text() {
        let text = extract_text("http://scrapethissite.com/pages/simple", ".country-name")
            .await
            .expect("could not find text");
        assert_eq!(text.as_str(), "Andorra");
    }
}
