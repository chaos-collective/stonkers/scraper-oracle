{ pkgs ? import <nixpkgs> { } }:
let manifest = (pkgs.lib.importTOML ./Cargo.toml).package;
in
pkgs.rustPlatform.buildRustPackage rec {
  pname = manifest.name;
  version = manifest.version;
  cargoLock.lockFile = ./Cargo.lock;
  src = pkgs.lib.cleanSource ./.;

  # build-time dependencies
  nativeBuildInputs = with pkgs; [
    pkg-config
  ];

  # run-time dependencies
  buildInputs = with pkgs; [
    firefox
    geckodriver
    openssl
    openssl.dev
  ];

  OPENSSL_DEV = pkgs.openssl.dev;
  PKG_CONFIG_PATH = "${pkgs.openssl.dev}/lib/pkgconfig";
  WEBDRIVER_PATH = "${pkgs.geckodriver}/bin/geckodriver";
  FIREFOX_PATH = "${pkgs.firefox}/bin/firefox";
}
